from django.contrib.auth.models import User

from django.contrib.auth import authenticate, login, logout
from django.shortcuts import get_object_or_404,redirect, render, render_to_response
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib import messages
from django.core.exceptions import PermissionDenied
from models import *
import json, requests
from django.contrib.auth.forms import *
from django.contrib import auth
import operator
import couchdb

# from django.utils import json
from dajaxice.decorators import dajaxice_register
#
# @dajaxice_register
# def sayhello(request):
#     return simplejson.dumps({'message':'Hello World'})

def calc_site(request):
    active = False
    try:
        list = get_servers_list()
        for row in list['rows']:
            if row['value']['operator'] == '+' and row['value']['host'] == 'http://djapp-g3nocide.rhcloud.com/apka/calc':
                active = True

        if not active:
            serv = couchdb.Server('http://194.29.175.241:5984')
            db = serv['calc']
            doc = {'host': 'http://djapp-g3nocide.rhcloud.com/apka/calc', 'active': True, 'operator': '+', 'calculation': 'http://djapp-g3nocide.rhcloud.com/apka/myop'}
            db.save(doc)
    except:
        pass

    return render(request, 'calc/calc.html', {
        'entry': 'fsad',
        })

def calc_my_op(request):

    number1 = 0;
    number2 = 0;
    if request.method == "POST" :
        number1 = request.POST['number1']
        number2 = request.POST['number2']
        response = HttpResponse(json.dumps(number1 + number2), mimetype='application/json')
        response['Access-Control-Allow-Origin'] = "*"
        return response

def test_add(a,b):
    addr = "http://djapp-g3nocide.rhcloud.com/apka/myop/"
    params = dict(
        number1 = a,
        number2 = b,
    )
    resp = requests.post(url=addr, params=params)
    data = resp.content
    print data


def get_servers_list():
    try:
        url = 'http://194.29.175.241:5984/calc/_design/utils/_view/list_active'
        resp = requests.get(url=url)
        data = json.loads(resp.content)
    except:
        data = False

    return data

def get_random_server(data, op):
    from random import choice
    list = []
    if not data:
        return False
    for row in data['rows']:
        if  row['value']['operator'] == op:
            list.append(row)
    if(len(list) > 0):
        random_choice = choice(list)
    else:
        random_choice = False
    return random_choice


def delete():
    rev = '1-e78bec19192a2855b1f2c8e49696091f'
    import httplib
    conn = httplib.HTTPConnection('http://194.29.175.241:5984')
    conn.request('DELETE', 'calc/_design/utils/list_active?rev='+rev, '')
    resp = conn.getresponse()
    content = resp.read()
    print content
    return 'test'