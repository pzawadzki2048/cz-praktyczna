__author__ = 'Genocide'

from celery import task
from views import get_random_server, get_servers_list

import operator
@task()
def calculate(string_inpt):
    stos = []
    znaki = ['+','-','*','/']
    ops = {'+': operator.add, '-': operator.sub, '/': operator.div, '*': operator.mul}
    data_ = get_servers_list()
    for char in string_inpt.split(' '):
        if ((char not in znaki) and (char != ' ')):
            stos.append(int(char))
        else:
            if(char in znaki):
                a = stos.pop()
                b = stos.pop()
                random_addr = get_random_addr(data_, char)
                operation_res = calculate_this(random_addr, a, b)
                if not operation_res:
                    random_addr = get_random_addr(data_, char)
                    operation_res = calculate_this(random_addr, a, b)
                    if not operation_res:
                        operation_res = ops[char](a,b)

                #return json.dumps({'message':json.dumps(addr)})
                stos.append(operation_res)

    result = stos
    try:
        wynik = result[0]
    except:
        wynik = 'error'
    return wynik


def calculate_this(addr, a, b):
    import uuid
    import json
    try:
        import requests
        _uuid = uuid.uuid4()
        params = dict(
            number1 = a,
            number2 = b,
            uuid = _uuid
        )
        resp = requests.post(url=addr, params=params)
        data = json.loads(resp.content)
    except:
        data = False

    return data

def split():
    serv = '/calculate/'
    serv = serv.rstrip('/')
    serv = serv.split('/')
    calculate = serv[len(serv) - 1]
    print calculate

def get_random_addr(data, char):
    randomserver = get_random_server(data, char)
    if not randomserver:
        return False
    serv = randomserver['value']['calculation']

    serv.rstrip("/")

    serv = serv.split('/')
    calculate = serv[len(serv) - 1]

    addr = 'http://' + randomserver['value']['host'].replace('http://', '') + '/' + calculate
    return addr