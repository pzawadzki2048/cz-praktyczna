from models import *
from django.contrib import admin




class EntryAdmin(admin.ModelAdmin):
    list_display = ('title', 'text', 'date')
    list_filter = ('date', )
    ordering = ('date', )
    search_fields = ('title',)

admin.site.register(Entry, EntryAdmin)