from django.db import models
from django.contrib.auth.models import User


class Entry(models.Model):
    title = models.CharField(max_length=40)
    text = models.CharField(max_length=400)
    date = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.title

class Comment(models.Model):
    content = models.CharField(max_length=100)
    author = models.CharField(max_length=100)
    subcomment = models.ForeignKey('self', null=True, blank=True)
    entry = models.ForeignKey(Entry)


    def __unicode__(self):
        return self.title