__author__ = 'Genocide'

import json
from dajaxice.decorators import dajaxice_register
from tasks import calculate
import operator
from views import get_random_server, get_servers_list
import uuid

@dajaxice_register
def sayhello(request, data):
    result = calculate.delay(str(data)).get()
    return json.dumps({'message' : result})


@dajaxice_register
def servers_list(request):
    data = get_servers_list()
    result = []
    if not data:
        return json.dumps({'message':'Server error', 'success': False})
    for elem in data['rows']:
        if elem['value']['active']:
            result.append(elem);
    return json.dumps({'message':result, 'success': True})

