from django.conf.urls import patterns, include, url
import os
from django.conf import settings
PROJECT_PATH = os.path.realpath(os.path.dirname(__file__))
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from dajaxice.core import dajaxice_autodiscover, dajaxice_config
dajaxice_autodiscover()
admin.autodiscover()

from django.contrib.staticfiles.urls import staticfiles_urlpatterns
urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'newproj.views.home', name='home'),

    url(r'^apka/', include('apka.urls')),

    url(r'^sors/(?P<path>.*)$', 'django.views.static.serve',
                         {'document_root': os.path.join(PROJECT_PATH, 'sors') + '/'}, name='staticfiles'),
    (r'^%s/' % settings.DAJAXICE_MEDIA_PREFIX, include('dajaxice.urls')),
    url(r'^admin/', include(admin.site.urls)),
)

urlpatterns += staticfiles_urlpatterns()
    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),

